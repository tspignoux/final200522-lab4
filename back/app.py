from fastapi import FastAPI
import uvicorn
from fastapi.middleware.cors import CORSMiddleware

import db

from api.fabricantes_api import fabricantes_router
from api.categorias_api import categorias_router
from api.muebles_api import muebles_router
from api.facturas_api import facturas_router
from api.pedidos_api import pedidos_router


from models.fabricante import Fabricante
from models.categoria import Categoria
from models.mueble import Mueble
from models.factura import Factura
from models.pedido import Pedido

app = FastAPI()

origins = [
    "http://localhost:8000",
    "http://localhost:3000",
    "http://localhost"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(fabricantes_router)
app.include_router(categorias_router)
app.include_router(muebles_router)
app.include_router(facturas_router)
app.include_router(pedidos_router)


#db.drop_all()
#db.create_all()

if __name__ == '__main__':
    uvicorn.run('app:app', reload=True)
