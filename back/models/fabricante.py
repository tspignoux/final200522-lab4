from email.mime import base
from sqlalchemy.sql.schema import ForeignKey
from db import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, BigInteger
from pydantic import BaseModel

class Fabricante(Base):
    __tablename__ = 'fabricantes'
    id = Column(Integer, primary_key = True)
    nombre = Column(String(50), nullable=False)
    direccion = Column(String(50))
    telefono = Column(BigInteger)
    contacto = Column(String(50))
    observaciones = Column(String(50))


class FabricantesModel(BaseModel):
    nombre: str
    direccion: str
    telefono: int
    contacto: str
    observaciones: str

    class Config:
        orm_mode = True

class FabricantesAPI(FabricantesModel):
    id: int