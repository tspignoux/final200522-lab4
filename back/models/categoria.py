from datetime import date
from email.mime import base
from enum import unique
from tokenize import Double
from sqlalchemy.sql.schema import ForeignKey
from db import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Date, Float, Integer, String, false
from pydantic import BaseModel

class Categoria(Base):
     __tablename__ = 'categorias'
     id = Column(Integer,nullable=False, primary_key = True , unique = True)
     categoria = Column(String(50), nullable = False)
     subcategoria = Column(String(50))
     

class CategoriasModel(BaseModel):
     categoria: str
     subcategoria: str


     class Config:
          orm_mode = True

class CategoriasAPI(CategoriasModel):
     id: int

     