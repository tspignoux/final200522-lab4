from datetime import date
from email.mime import base
from tokenize import Double
from xmlrpc.client import Boolean
from sqlalchemy.sql.schema import ForeignKey
from db import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Date, Float, Integer, String, false, Boolean
from pydantic import BaseModel

class Pedido(Base):
    __tablename__ = 'pedidos'
    nrop = Column(Integer, primary_key = True)
    fecha = Column(Date, nullable = False)
    fecha_arribo = Column(Date)
    cantidad = Column(Integer, nullable = False, default = 1)
    estado = Column(Integer, default = 0)
    id_categoria = Column(ForeignKey('categorias.id'), nullable=False)
    categoria = relationship('Categoria', lazy='joined')
    id_fabricante = Column(ForeignKey('fabricantes.id'), nullable=False)
    fabricante = relationship('Fabricante', lazy='joined')

class PedidosModel(BaseModel):
    fecha: date
    fecha_arribo: date
    cantidad: int
    id_categoria: int
    id_fabricante: int

    class Config:
        orm_mode = True

class PedidosAPI(PedidosModel):
    nrop: int
