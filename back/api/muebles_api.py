from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.muebles_repo import MueblesRepo
from models.mueble import Mueble, MueblesModel, MueblesAPI

muebles_router = APIRouter(prefix='/muebles', tags=['Muebles'])
repo = MueblesRepo()

@muebles_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session)

@muebles_router.get('/disponibles')
def get_disponibles(session: Session = Depends(get_session)):
    return repo.get_disponibles(session) 

@muebles_router.get('/fabricantes/{id_fabricante}')
def get_by_fabricantes(id_fabricante, session: Session = Depends(get_session)):
    return repo.get_by_fabricantes(id_fabricante, session)

@muebles_router.get('/categorias/{id_categoria}')
def get_by_categoria(id_categoria: int, session: Session = Depends(get_session)):
    return repo.get_by_categoria(id_categoria, session)

@muebles_router.get('/{nro_serie}')
def get_by_serial(nro_serie: str, session: Session = Depends(get_session)):
    mueble = repo.get_by_serial(nro_serie, session)
    if mueble:
        return mueble
    else:
        raise HTTPException(status_code=404, detail="Mueble no existente")




@muebles_router.get('/stock/{id_categoria}')
def contar_stock(id_categoria: int, session: Session = Depends(get_session)):
    return repo.contar_stock(id_categoria, session)

@muebles_router.post('/', response_model=MueblesAPI)
def set_mueble(data: MueblesModel, session: Session = Depends(get_session)):
    mueble = repo.set_mueble(data, session)
    return mueble

@muebles_router.delete('/{nro_serie}')
def delete_mueble(nro_serie: str, session: Session = Depends(get_session)):
    result = repo.delete_mueble(nro_serie, session)
    if not result:
        raise HTTPException(status_code=404, detail="Mueble no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente el Mueble")

@muebles_router.put('/{nro_serie}', response_model=MueblesAPI)
def update_mueble(nro_serie: str, data: MueblesModel, session: Session = Depends(get_session)):
    mueble = repo.update_mueble(nro_serie, data, session)
    if not mueble:
        raise HTTPException(status_code=404, detail="Mueble no existente")
    else:
        return mueble

""" @muebles_router.patch('/precio/{nro_serie}/{nprecio}', response_model= MueblesAPI)
def precio_mueble(nro_serie: str, nprecio: int, session: Session = Depends(get_session)):
    mueble = repo.precio_mueble(nro_serie, nprecio, session)
    if not mueble:
        raise HTTPException(status_code=404, detail="Mueble no existente")
    else:
        return mueble """

""" @muebles_router.patch('/sell/{nro_serie}', response_model= MueblesAPI)
def sell_mueble(nro_serie: str, session: Session = Depends(get_session)):
    mueble = repo.sell_mueble(nro_serie, session)
    if not mueble:
        raise HTTPException(status_code=404, detail="Mueble no existente")
    else:
        return mueble """

""" @muebles_router.patch('/back/{nro_serie}', response_model= MueblesAPI)
def back_mueble(nro_serie: str, session: Session = Depends(get_session)):
    mueble = repo.back_mueble(nro_serie, session)
    if not mueble:
        raise HTTPException(status_code=404, detail="Mueble no existente")
    else:
        return mueble """
