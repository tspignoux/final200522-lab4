from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
from db import get_session
from repository.categorias_repo import CategoriasRepo
from models.categoria import CategoriasModel, CategoriasAPI

categorias_router = APIRouter(prefix='/categorias', tags=['Categorias'])
repo = CategoriasRepo()

@categorias_router.get('/')
def get_all(session: Session = Depends(get_session)):
    return repo.get_all(session) 

@categorias_router.get('/search/{name}')
def get_by_name(name, session: Session = Depends(get_session)):
    return repo.get_by_name(name, session)

@categorias_router.get('/{id}')
def get_by_id(id, session: Session = Depends(get_session)):
    categoria = repo.get_by_id(id, session)
    if categoria:
        return categoria
    else:
        raise HTTPException(status_code=404, detail="Categoria no existente")

@categorias_router.post('/', response_model=CategoriasAPI)
def set_categoria(data: CategoriasModel, session: Session = Depends(get_session)):
    categoria = repo.set_categoria(data, session)
    return categoria

@categorias_router.delete('/{id}')
def delete_categoria(id, session: Session = Depends(get_session)):
    result = repo.delete_categoria(id, session)
    if not result:
        raise HTTPException(status_code=404, detail="Categoria no existente")
    else:
        raise HTTPException(status_code=200, detail="Se elimino correctamente esta categoria")

@categorias_router.put('/{id}', response_model=CategoriasAPI)
def update_categoria(id, data: CategoriasModel, session: Session = Depends(get_session)):
    categoria = repo.update_categoria(id, data, session)
    if not categoria:
        raise HTTPException(status_code=404, detail="Categoria no existente")
    else:
        return categoria